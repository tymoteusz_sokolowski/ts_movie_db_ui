#Movie DB Search UI

## Installation instructions

```
# install dependencies
yarn install

# serve application at localhost:3000
yarn start
```

## Run tests
```
yarn test
```

## Build production version
```
yarn build
```

Credits:

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Demo of this project is available online: http://tymoteuszsokolowski.eu/moviedbui/