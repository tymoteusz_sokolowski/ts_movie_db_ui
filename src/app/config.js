export const config = {
    minimalSearchPhraseLength: 3,
    photoFormat: 'w320',
    originalFormat: 'original',
    startPhoto: '/1Hq659V31ER9pQQvVBUUJbwR7NT.jpg',
    api_key: '4ff53d185d6a13b1986999e2a45b42bb',
    base_uri: 'https://api.themoviedb.org/3/',
    images_uri: 'https://image.tmdb.org/t/p/',
}
