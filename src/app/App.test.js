import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {shallow} from 'enzyme';

import {Header} from './components/Header/Header';
import {SearchInput} from "./components/Search/SearchInput/SearchInput";
import {Results} from "./components/Results/Results";
import Searcher from "./components/Search/Searcher/Searcher";
import {Background} from './components/Background/Background';

describe('<App />', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<App />, div);
    });

    it('should render <Header /> component', () => {
        const wrapper = shallow(<App />);
        expect(wrapper.find(Header).length).toBe(1);
    });

    it('should render <SearchInput /> component', () => {
        const wrapper = shallow(<App />);
        expect(wrapper.find(SearchInput).length).toBe(1);
    });

    it('should render <Searcher /> component', () => {
        const wrapper = shallow(<App />);
        expect(wrapper.find(Searcher).length).toBe(1);
    });

    it('should render <Background /> component', () => {
        const wrapper = shallow(<App />);
        expect(wrapper.find(Background).length).toBe(1);
    });

    it('initially should NOT render <Results /> component', () => {
        const wrapper = shallow(<App />);
        expect(wrapper.find(Results).length).toBe(0);
    });

    it('shows <Results /> component when resultsData is filled', () => {
        const wrapper = shallow(<App />);
        wrapper.setState({ resultsData: [] });
        expect(wrapper.find(Results).length).toBe(1);
    });

});
