import React from "react";
import sinon from 'sinon';

import { MovieDBService } from './MovieDB.service';

describe('MovieDBService', () => {
    let query = 'some query';
    let movieDB = {};

    movieDB.searchMovie = sinon.stub();
    movieDB.searchMovie.returns([]);

    let movieDBService = new MovieDBService(movieDB);

    it('should pass query to movieDB.searchMovie', () => {
        movieDBService.searchMovie(query);
        sinon.assert.calledWith(movieDB.searchMovie, { query });
    });

    it('should return promise', () => {
        expect(movieDBService.searchMovie(query) instanceof Promise).toBe(true);
    });
});

