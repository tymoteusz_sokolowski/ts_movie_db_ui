import { MovieModel } from '../models/MovieModel';
import { MovieCollection } from '../models/MovieCollection';

export class MovieDBService {

    constructor (movieDB) {
        this.movieDB = movieDB;
    }

    _fillMovieCollection(results) {
        let movieCollection = new MovieCollection();

        for (let movie of results) {
            movieCollection.addMovie(new MovieModel(movie))
        }

        return movieCollection;
    }

    searchMovie(query) {
        return new Promise((resolve, reject) => {
            this.movieDB.searchMovie({ query: query }, (err, res) => {
                resolve (this._fillMovieCollection(res.results));
            });
        });
    }
}

