import React from "react";
import logo from '../../assets/logo.svg';

export const Header = () => {
    return(
        <div className="App-header">
            <div className="App-header-background"></div>
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Movie Search</h1>
        </div>
    )
};
