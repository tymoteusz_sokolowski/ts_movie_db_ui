import React from "react";
import {shallow} from 'enzyme';
import { Header } from './Header';

describe('<Header />', () => {

    it('should render header background', () => {
        const wrapper = shallow(<Header />);
        expect(wrapper.find('.App-header-background').length).toBe(1);
    });

    it('should render logo image', () => {
        const wrapper = shallow(<Header />);
        expect(wrapper.find('.App-logo').length).toBe(1);
    });

    it('should render title', () => {
        const wrapper = shallow(<Header />);
        expect(wrapper.find('.App-title').length).toBe(1);
    });
});
