import React from "react";
import { MovieCard } from './MovieCard/MovieCard';

export const Results = (props) => {
    return(
        <div className="App-results">
                { props.resultsData.map((record, key) => <MovieCard movieData={record} id={key} key={key} /> )}
        </div>
    )
};
