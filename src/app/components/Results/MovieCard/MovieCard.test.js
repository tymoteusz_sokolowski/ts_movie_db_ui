import React from "react";
import {shallow} from 'enzyme';
import { MovieCard } from './MovieCard';

describe('<MovieCard />', () => {

    let record = {};
    it('should render MoviePicture', () => {
        const wrapper = shallow(<MovieCard movieData={record} />);
        expect(wrapper.find('.MoviePicture').length).toBe(1);
    });

    it('should render MovieTitle', () => {
        const wrapper = shallow(<MovieCard movieData={record} />);
        expect(wrapper.find('.MovieTitle').length).toBe(1);
    });

    it('should render MovieOverview', () => {
        const wrapper = shallow(<MovieCard movieData={record} />);
        expect(wrapper.find('.MovieOverview').length).toBe(1);
    });

    it('should render MovieReleaseDate', () => {
        const wrapper = shallow(<MovieCard movieData={record} />);
        expect(wrapper.find('.MovieReleaseDate').length).toBe(1);
    });

    it('should render MovieAverage', () => {
        const wrapper = shallow(<MovieCard movieData={record} />);
        expect(wrapper.find('.MovieAverage').length).toBe(1);
    });

    it('should render MovieAverage', () => {
        const wrapper = shallow(<MovieCard movieData={record} />);
        expect(wrapper.find('.MovieAverage').length).toBe(1);
    });
});
