import React from "react";
import PropTypes from 'prop-types';

export class MovieCard extends React.Component {
    render () {
        return (
            <div className="MovieCard">
                <div className="MoviePicture"><img
                        src={this.props.movieData.poster_url}
                        alt={this.props.movieData.title}
                    /></div>
                <div className="MovieDescription">
                    <div className="MovieTitle">
                        <h2>{this.props.movieData.title}</h2>
                        {(this.props.movieData.original_title !== this.props.movieData.title) ? <h3>{this.props.movieData.original_title}</h3> : ''}
                    </div>
                    <div className="MovieOverview">{this.props.movieData.overview}</div>
                    <div className="MovieReleaseDate">
                        <div className="Caption">Release Date:</div>
                        <div className="Value">{this.props.movieData.release_date}</div>
                    </div>
                    <div className="MovieAverage">
                        <div className="Caption">Vote average</div>
                        <div className="Value">{this.props.movieData.vote_average}</div>
                    </div>
                </div>
            </div>
        )
    }
}

MovieCard.propTypes = {
    movieData: PropTypes.object
};
