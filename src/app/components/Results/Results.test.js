import React from "react";
import {shallow} from 'enzyme';
import { Results } from './Results';
import { MovieCard } from './MovieCard/MovieCard';

describe('<Results />', () => {

    it('should render component <div /> with class .App-results', () => {
        const wrapper = shallow(<Results resultsData={ [] } />);
        expect(wrapper.find('.App-results').length).toBe(1);
    });

    it('should render same amount of <MovieCard /> components as given records in resultsData', () => {
        let someResultsData = [{}, {}, {}];
        const wrapper = shallow(<Results resultsData={ someResultsData } />);
        expect(wrapper.find(MovieCard).length).toBe(someResultsData.length);
    });

});
