import React from "react";
import {shallow} from 'enzyme';
import { Background } from './Background';

describe('<Background />', () => {

    it('should render <div /> element with class .App-background', () => {
        const wrapper = shallow(<Background />);
        expect(wrapper.find('.App-background').length).toBe(1);
    });
});
