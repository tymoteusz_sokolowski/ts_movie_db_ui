import React from "react";
import PropTypes from 'prop-types';
import { config } from '../../config';

export class Background extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            imgUrl: ''
        };
    }

    fullPath(url) {
        return config.images_uri+config.originalFormat+url;
    }

    getBackgroundStyle() {
        return {
            backgroundImage: 'url(' + this.props.imgUrl + ')'
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.imgUrl === nextProps.imgUrl) {
            return;
        }
        this.setState({
            imgUrl: nextProps.imgUrl,
        });
    }

    render () {
        return (<div className="App-background" style={this.getBackgroundStyle()} />)
    }
}

Background.propTypes = {
    imgUrl: PropTypes.string
};