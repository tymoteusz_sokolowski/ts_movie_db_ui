import React from "react";
import PropTypes from 'prop-types';
import { config } from '../../../config.js';

export class SearchInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchPhrase: ''
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        let changedValue = event.target.value;
        this.setState({searchPhrase: changedValue}, this.updateAppState);
    }

    updateAppState() {
        if (this.isPhraseLongEnough()) {
            this.props.onChangePhrase(this.state.searchPhrase);
        }
    }

    isPhraseLongEnough() {
        return (this.state.searchPhrase.length >= config.minimalSearchPhraseLength);
    }

    render() {
        return (
            <div className="App-search">
                <input
                    type="text"
                    placeholder="enter movie to find"
                    className="Search-input"
                    onChange={this.handleChange}
                    value={this.state.searchPhrase}
                />
            </div>
        )
    }
}

SearchInput.propTypes = {
    onChangePhrase: PropTypes.func
};