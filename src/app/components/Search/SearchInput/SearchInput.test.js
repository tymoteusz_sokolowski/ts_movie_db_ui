import React from "react";
import { mount } from 'enzyme';
import sinon from 'sinon';
import {shallow} from 'enzyme';

import { SearchInput } from './SearchInput';

import { config } from '../../../config.js';

const minLength = config.minimalSearchPhraseLength;
const toShortString = 'a'.repeat(minLength-1);
const longEnoughString = 'a'.repeat(minLength);

describe('<SearchInput />', () => {

    it('should render <input /> element with class .Search-input', () => {
        const wrapper = shallow(<SearchInput />);
        expect(wrapper.find('.Search-input').length).toBe(1);
    });

    it('when user enter search phrase shorter than ' + minLength + ' characters does not emit event onChangeSearchPhrase', () => {
        const onChangePhrase = sinon.spy();
        const wrapper = mount(<SearchInput onChangePhrase={onChangePhrase}/>);
        const inputElement = wrapper.find('.Search-input').first();

        inputElement.simulate('change', {target: {value: toShortString}});

        expect(onChangePhrase.called).toBe(false);
    });


    it('when user enter search phrase at least ' + minLength + ' characters long emits event onChangeSearchPhrase', () => {
        const onChangePhrase = sinon.spy();
        const wrapper = mount(<SearchInput onChangePhrase={onChangePhrase}/>);
        const inputElement = wrapper.find('.Search-input').first();

        inputElement.simulate('change', {target: {value: longEnoughString}});

        expect(onChangePhrase.called).toBe(true);
    });

});