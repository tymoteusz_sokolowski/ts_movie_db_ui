import React from "react";
import { mount } from 'enzyme';
import sinon from 'sinon';
import Searcher from './Searcher';
import { register } from '../../../di';

const movieDBService = {};
register('movieDBService', movieDBService);

describe('<Searcher/>', () => {
    let searchPhrase = '';
    const onSearchDone = sinon.spy();
    const some_data = 'some data';
    const some_search_phrase = 'some search phrase';

    it('should called movieDBService.searchMovie when search phrase will change,', () => {
        searchPhrase = '';
        movieDBService.searchMovie = sinon.stub();
        movieDBService.searchMovie.returns(new Promise(() => {}));

        const wrapper = mount(<Searcher searchPhrase={searchPhrase}  onSearchDone={onSearchDone}/>);

        expect(movieDBService.searchMovie.called).toBe(false);
        wrapper.setProps({ searchPhrase: some_search_phrase });
        expect(movieDBService.searchMovie.called).toBe(true);
    });

    it('should not called movieDBService.searchMovie  when new search phrase is same as previous,', () => {
        searchPhrase = '';
        movieDBService.searchMovie = sinon.stub();
        movieDBService.searchMovie.returns(new Promise(() => {}));

        const wrapper = mount(<Searcher searchPhrase={searchPhrase}  onSearchDone={onSearchDone}/>);

        wrapper.setProps({ searchPhrase: some_search_phrase });
        wrapper.setProps({ searchPhrase: some_search_phrase });
        expect(movieDBService.searchMovie.calledOnce).toBe(true);
        expect(movieDBService.searchMovie.calledTwice).toBe(false);
    });

    it('should send search phrase to movieDBService.searchMovie', () => {

        movieDBService.searchMovie = (query) => {
            return new Promise((resolve) => {
                expect(query).toEqual(some_search_phrase);
                resolve(some_data);
            });
        };
        searchPhrase = '';
        const wrapper = mount(<Searcher searchPhrase={searchPhrase}  onSearchDone={onSearchDone}/>);
        wrapper.setProps({ searchPhrase: some_search_phrase });
    });

    it('pass data to main component when receive data from MovieDB.searchMovie', () => {
        searchPhrase = '';
        const wrapper = mount(<Searcher searchPhrase={searchPhrase}  onSearchDone={onSearchDone}/>);
        wrapper.setProps({ searchPhrase: some_search_phrase });
        sinon.assert.calledWith(onSearchDone, some_data);
    });
});