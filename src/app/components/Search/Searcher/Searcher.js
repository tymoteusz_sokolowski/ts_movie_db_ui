import React from "react";
import PropTypes from 'prop-types';
import { wire } from '../../../di';

export class Searcher extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showSearchingInfo: false,
            searchPhrase: ''
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.searchPhrase === nextProps.searchPhrase) {
            return;
        }
        this.setState({
            searchPhrase: nextProps.searchPhrase,
            showSearchingInfo: true
        });

        this.props.db.searchMovie(nextProps.searchPhrase).then((res) => {
            this.setState({
                showSearchingInfo: false
            });
            this.props.onSearchDone(res);
        });
    }

    render () {
        return (<div className="Searcher">
            { this.state.showSearchingInfo ? <p className="search-phrase">Searching for {this.props.searchPhrase}</p> : null }
        </div>)
    }
}

Searcher.propTypes = {
    searchPhrase: PropTypes.string,
    onSearchDone: PropTypes.func
};

export default wire(Searcher, ['movieDBService'], db => ({ db }));