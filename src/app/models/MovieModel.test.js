import noPosterAvailable from '../assets/noPosterAvailable.png';
import { MovieModel } from './MovieModel'

describe('MovieModel', () => {

    describe('poster_path ', () => {
        it('is equal to noPosterAvailable, when poster_path is empty ', () => {

            let movieData = {};
            let movieModel = new MovieModel(movieData);

            expect(movieModel.poster_url).toBe(noPosterAvailable);
        });

        it('contains poster_path from movieData, when is not empty ', () => {

            let somePath = '/some_path';
            let movieData = {
                poster_path: somePath
            };
            let movieModel = new MovieModel(movieData);

            expect(movieModel.poster_url.indexOf(somePath)>=0).toBe(true);
        });
    });

    describe('background_url', () => {
        it('is properly composited, when poster_path is empty ', () => {

            let movieData = {};
            let movieModel = new MovieModel(movieData);
            let backgroundUrlForEmptyPoster = "https://image.tmdb.org/t/p/original/1Hq659V31ER9pQQvVBUUJbwR7NT.jpg";

            expect(movieModel.background_url).toBe(backgroundUrlForEmptyPoster);
        });

        it('contains poster_path from movieData, when is not empty ', () => {

            let somePath = '/some_path';
            let movieData = {
                poster_path: somePath
            };
            let movieModel = new MovieModel(movieData);

            expect(movieModel.background_url.indexOf(somePath)>=0).toBe(true);
        });
    });

});
