import { config } from '../config';
import noPosterAvailable from '../assets/noPosterAvailable.png';

export class MovieModel {

    constructor (movieData = {}) {
        this._raw_poster_path = movieData.poster_path;
        this.title = movieData.title;
        this.original_title = movieData.original_title;
        this.overview = movieData.overview;
        this.release_date = movieData.release_date;
        this.vote_average = movieData.vote_average;
    }

    get poster_url() {
        if (!this._raw_poster_path) {
            return noPosterAvailable;
        }
        return config.images_uri + config.photoFormat + this._raw_poster_path;
    }

    get background_url() {
        if (!this._raw_poster_path) {
            return  config.images_uri + config.originalFormat + config.startPhoto;
        }
        return config.images_uri + config.originalFormat + this._raw_poster_path;
    }
}
