export class MovieCollection {

    constructor () {
        this.movies = [];
    }

    addMovie(movie) {
        this.movies.push(movie);
    }
}
