import React, { Component } from 'react';
import './App.css';
import { config } from './config';
import { Header } from './components/Header/Header';
import { SearchInput } from "./components/Search/SearchInput/SearchInput";
import { Results } from "./components/Results/Results";
import { register } from './di';
import Searcher from "./components/Search/Searcher/Searcher";
import { Background } from './components/Background/Background';
import { MovieDBService } from './services/MovieDB.service';

const MovieDB = require('moviedb')(config.api_key);
const movieDBService = new MovieDBService(MovieDB);

register('movieDBService', movieDBService);

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchPhrase: '',
            resultsData: null,
            imgUrl: config.images_uri + config.originalFormat +  config.startPhoto

        };
        this.onChangeSearchPhrase = this.onChangeSearchPhrase.bind(this);
        this.onSearchDone = this.onSearchDone.bind(this);
    }

    onChangeSearchPhrase(changedValue) {
        this.setState({searchPhrase: changedValue});
    }

    onSearchDone(results) {
        let movies = results.movies;
        this.setState({
            resultsData: movies,
            imgUrl: movies[0].background_url
        });
    }

    render() {
        return (
            <div className="App">
                <Background imgUrl={this.state.imgUrl} />
                <Header/>
                <SearchInput onChangePhrase={this.onChangeSearchPhrase}/>
                <Searcher searchPhrase={this.state.searchPhrase} onSearchDone={this.onSearchDone} />
                { this.state.resultsData ? <Results resultsData={this.state.resultsData} /> : ''}
            </div>
        );
    }
}

export default App;
